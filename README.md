# Séance ElementR du 21 mai 2024 

### Manipulation de données géographiques raster avec R

#### Dynamiques hydrologiques dans l’Okavango

##### Malika Madelin & Paul Passy


Support du TP à ce [lien](https://elementr-raster-elementr-session-geomatique-eefbb27c2522efe93ff.gitpages.huma-num.fr/)
